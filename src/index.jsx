import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { IntlProvider } from 'react-intl';

import store from '~/store';

// import * as serviceWorker from './serviceWorker';

const messagesInFrench = {};

const render = () => {
  // eslint-disable-next-line global-require
  const App = require('./app/App').default;

  ReactDOM.render(
    <Provider store={store}>
      <IntlProvider messages={messagesInFrench} locale="en" defaultLocale="en">
        <App />
      </IntlProvider>
    </Provider>,
    document.getElementById('root')
  );
};

render();

if (process.env.NODE_ENV === 'development' && module.hot) {
  module.hot.accept('./app/App', render);
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
