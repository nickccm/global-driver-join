import React from 'react';
import styled from 'styled-components';

const TextInput = styled.input`
  padding: 1em;
  border: 1px solid #000;
  background-color: transparent;
  resize: none;
  outline: none;
`;

const Input = (props) => {
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <TextInput {...props} />;
};

export default Input;
