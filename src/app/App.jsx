import React from 'react';
import { hot } from 'react-hot-loader/root';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { createGlobalStyle } from 'styled-components';

import Auth from '~/screens/Auth';
import AccountInfo from '~/screens/Step1';
import Verification from '~/screens/Step1-1';
import PersonalInfo from '~/screens/Step2';
import VehicleChoice from '~/screens/Step3';
import VehicleInfo from '~/screens/Step3-1';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen',
      'Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue',
      sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New',
      monospace;
  }
`;

function App() {
  return (
    <>
      <GlobalStyle />
      <BrowserRouter>
        <Switch>
          <Route path="/" exact>
            <Auth />
          </Route>
          <Route path="/step1" exact>
            <AccountInfo />
          </Route>
          <Route path="/step1-1" exact>
            <Verification />
          </Route>
          <Route path="/step2" exact>
            <PersonalInfo />
          </Route>
          <Route path="/step3" exact>
            <VehicleChoice />
          </Route>
          <Route path="/step3-1" exact>
            <VehicleInfo />
          </Route>
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default hot(App);
