import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

const VCodeContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 50%;
  margin: auto;
  justify-content: space-evenly;
`;
const VCodeInput = styled.input`
  width: 20px;
  height: 20px;
  display: inline-block;
  border: 5px solid black;
`;

const StyledLink = styled(Link)`
  margin: auto;
  display: block;

  .sign-up {
    margin: auto;
    display: block;
  }
`;

const Verification = () => {
  return (
    <>
      <h1>Verify your mobile number</h1>
      <VCodeContainer>
        <VCodeInput type="number" min="0" max="9" />
        <VCodeInput type="number" min="0" max="9" />
        <VCodeInput type="number" min="0" max="9" />
        <VCodeInput type="number" min="0" max="9" />
      </VCodeContainer>
      <StyledLink to="/step2">
        <button className="sign-up" type="button">
          Sign Up
        </button>
      </StyledLink>
    </>
  );
};

export default Verification;
