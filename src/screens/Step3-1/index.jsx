import React from 'react';
import { Link } from 'react-router-dom';

const VehicleInfo = () => {
  return (
    <>
      <h1>Tell us about your vehicle and driving information</h1>
      <Link to="/">
        <button type="button">Continue</button>
      </Link>
    </>
  );
};

export default VehicleInfo;
