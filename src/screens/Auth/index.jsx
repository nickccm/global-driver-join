import React from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';

import { changePhone } from '~/store/modules/auth/actions';
import PhoneInput from '~/components/PhoneInput';
import logo from '~/assets/logo.svg';

const Container = styled.div`
  text-align: center;
`;

const Header = styled.header`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  min-height: 100vh;
  color: #000;
  font-size: calc(10px + 2vmin);
`;

const LogoImg = styled.img`
  height: 40vmin;
  pointer-events: none;
`;

const Subtitle = styled.span`
  color: #000;
`;

const Auth = () => {
  const dispatch = useDispatch();
  const { phone } = useSelector((state) => state.auth);
  return (
    <>
      <Container>
        <FormattedMessage
          description="Today's message"
          defaultMessage="Today is {ts, date, ::yyyyMMdd}"
          values={{ ts: Date.now() }}
        />
        <br />
        <FormattedMessage
          description="Greeting"
          defaultMessage="Hello, {name}"
          values={{ name: 'Nick' }}
        />
        <Header>
          <LogoImg src={logo} alt="logo" />
          <p>Deliver Faster!</p>
          <Subtitle>Sign in or create an account</Subtitle>
          <PhoneInput
            value={phone}
            onChange={(e) => dispatch(changePhone({ phone: e.target.value }))}
          />
        </Header>
      </Container>
    </>
  );
};

export default Auth;
