import React from 'react';
import { Link } from 'react-router-dom';

const AccountInfo = () => {
  return (
    <>
      <h1>Account info</h1>
      <Link to="/step1-1">
        <button type="button">Sign Up</button>
      </Link>
    </>
  );
};

export default AccountInfo;
