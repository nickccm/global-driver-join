import React from 'react';
import { Link } from 'react-router-dom';

const PersonalInfo = () => {
  return (
    <>
      <h1>Tell us about yourself</h1>
      <Link to="/step3">
        <button type="button">Sign Up</button>
      </Link>
    </>
  );
};

export default PersonalInfo;
