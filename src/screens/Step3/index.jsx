import React from 'react';
import { Link } from 'react-router-dom';

const VehicleChoice = () => {
  return (
    <>
      <h1>How will you deliver</h1>
      <Link to="/step3-1">
        <button type="button">Continue</button>
      </Link>
    </>
  );
};

export default VehicleChoice;
