import { combineReducers } from '@reduxjs/toolkit';

import auth from './modules/auth/reducer';
import step1 from './modules/step1/reducer';


const rootReducer = combineReducers({
  auth,
  step1,
});

export default rootReducer;
