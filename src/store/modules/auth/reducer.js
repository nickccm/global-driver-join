import { createReducer } from '@reduxjs/toolkit';

import { changePhone, changeAreaCode } from './actions';

const INIT_STATE = {
  areaCode: '',
  phone: '',
};

const authReducer = createReducer(INIT_STATE, {
  [changePhone]: (state, action) => {
    return {
      ...state,
      phone: action.payload.phone,
    };
  },
  [changeAreaCode]: (state, action) => {
    return {
      ...state,
      areaCode: action.payload.areaCode,
    };
  },
});

export default authReducer;
