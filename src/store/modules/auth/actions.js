import { createAction } from '@reduxjs/toolkit';

export const changePhone = createAction('CHANGE_PHONE');
export const changeAreaCode = createAction('CHANGE_AREA_CODE');
