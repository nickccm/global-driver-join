import { createReducer } from '@reduxjs/toolkit';

import {
  changeFullName,
  changeCountryPhoneCode,
  changeMobilePhoneNumber,
  changePassword,
  changeCityYouWillDriveIn,
  changeEmail,
  changeReferralCode,
  changeIAgreeToTermsAndConditions
 } from './actions';

const INIT_STATE = {
  fullName: '',
  countryPhoneCode: '',
  mobilePhoneNumber: '',
  password: '',
  cityYouWillDriveIn: '',
  email: '',
  referralCode:'',
  iAgreeToTermsAndConditions: false
};

const step1Reducer = createReducer(INIT_STATE, {
  [changeFullName]: (state, action) => {
    return {
      ...state,
      fullName: action.payload.fullName,
    };
  },
  [changeCountryPhoneCode]: (state, action) => {
    return {
      ...state,
      countryPhoneCode: action.payload.countryPhoneCode,
    };
  },
  [changeMobilePhoneNumber]: (state, action) => {
    return {
      ...state,
      mobilePhoneNumber: action.payload.mobilePhoneNumber,
    };
  },
  [changePassword]: (state, action) => {
    return {
      ...state,
      password: action.payload.password,
    };
  },
  [changeCityYouWillDriveIn]: (state, action) => {
    return {
      ...state,
      cityYouWillDriveIn: action.payload.cityYouWillDriveIn,
    };
  },
  [changeEmail]: (state, action) => {
    return {
      ...state,
      email: action.payload.email,
    };
  },
  [changeReferralCode]: (state, action) => {
    return {
      ...state,
      referralCode: action.payload.referralCode,
    };
  },
  [changeIAgreeToTermsAndConditions]: (state, action) => {
    return {
      ...state,
      iAgreeToTermsAndConditions: action.payload.iAgreeToTermsAndConditions,
    };
  },
});

export default step1Reducer;
