import { createAction } from '@reduxjs/toolkit';

export const changeFullName = createAction('CHANGE_FULL_NAME');
export const changeCountryPhoneCode = createAction('CHANGE_COUNTRY_PHONE_CODE'); //DO WE NEED TO HAVE AN ABILITY TO CHANGE country phone code HERE?
export const changeMobilePhoneNumber = createAction('CHANGE_MOBILE_PHONE_NUMBER');
export const changePassword = createAction('CHANGE_PASSWORD');
export const changeCityYouWillDriveIn = createAction('CHANGE_CITY_YOU_WILL_DRIVE_IN');
export const changeEmail = createAction('CHANGE_EMAIL');
export const changeReferralCode = createAction('CHANGE_REFERRAL_CODE');
export const changeIAgreeToTermsAndConditions = createAction('CHANGE_I_AGREE_TO_TERMS_AND_CONDITIONS');
